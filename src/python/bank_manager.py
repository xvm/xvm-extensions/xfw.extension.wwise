"""
SPDX-License-Identifier: MIT
Copyright (c) 2020-2024 XVM Team
"""

#
# Includes
#

# stdlib
import logging

# BigWorld
from PlayerEvents import g_playerEvents

# XFW.Loader
import xfw_loader.python as xfw_loader

# XFW.WWise
import native



#
# Globals
#

g_xfw_wwise_bankmanager = None
g_xfw_wwise_bankmanager_inited = False



#
# BankManager
#

class BankManager(object):
    STATE_HANGAR  = 1
    STATE_BATTLE  = 2

    # Init

    def __init__(self, native):
        self.__logger = logging.getLogger('XFW/WWISE')

        self.__native = native

        self.__banks_battle = set()
        self.__banks_hangar = set()

        self.__banks_loaded = dict()
        self.__state = BankManager.STATE_HANGAR

        g_playerEvents.onAvatarBecomePlayer += self.__on_avatar_become_player
        g_playerEvents.onAvatarBecomeNonPlayer += self.__on_avatar_become_non_player

    def __fini__(self):
        g_playerEvents.onAvatarBecomePlayer -= self.__on_avatar_become_player
        g_playerEvents.onAvatarBecomeNonPlayer -= self.__on_avatar_become_non_player


    def is_initialized(self):
        return self.__native is not None


    # Banks

    def bank_add(self, bank_path, add_to_battle, add_to_hangar):
        """
        Add bank to loading list.
        Use reload() to perform bank load.

        bank_path     -- path to bank relative to ../res_mods/x.x.x/audioww/
        add_to_battle -- true to load bank in battle
        add_to_hangar -- true to load bank in hangar
        _from_config  -- do not use this
        """
        normalized_path = self.__normalize_path(bank_path)
        if add_to_battle:
            self.__banks_battle.add(normalized_path)

        if add_to_hangar:
            self.__banks_hangar.add(normalized_path)

        self.__banks_reload()
        return True


    def bank_remove(self, bank_path):
        """
        Remove bank from loading list.
        Use reload() to perform bank unload.

        bank_path          -- path to bank relative to ../res_mods/x.x.x/audioww/
        """
        normalized_path = self.__normalize_path(bank_path)

        if bank_path in self.__banks_battle:
            self.__banks_battle.remove(normalized_path)

        if bank_path in self.__banks_hangar:
            self.__banks_hangar.remove(normalized_path)

        self.__banks_reload()
        return True
    
    def bank_unload(self, bank_name):
        """
        Unloads bank by bank name
        """
        
        if self.__native is None:
            self.__logger.warning('banks_reload: native module is not initialized')
            return False
        
        return self.__native.bank_unload_by_name(bank_name)


    def __banks_reload(self):
        """
        Perform banks load and unload
        """

        if self.__native is None:
            self.__logger.warning('banks_reload: native module is not initialized')
            return

        banks_to_load = set()
        banks_to_unload = set()

        if self.__state == BankManager.STATE_HANGAR:
            banks_to_load = self.__banks_hangar
        elif self.__state == BankManager.STATE_BATTLE:
            banks_to_load = self.__banks_battle
        else:
            logging.warn('__banks_reload(): unknown state')
            return

        for key in self.__banks_loaded.iterkeys():
            if key not in banks_to_load:
                banks_to_unload.add(key)

        for bank_path in banks_to_unload:
            bank_id = self.__banks_loaded.pop(bank_path)
            if bank_id:
                self.__native.bank_unload(bank_id)

        for bank_path in banks_to_load:
            if bank_path not in self.__banks_loaded:
                bank_id = self.__native.bank_load(unicode(bank_path), unicode(xfw_loader.WOT_RESMODS_DIR + '/audioww/'))
                if bank_id:
                    self.__banks_loaded[bank_path] = bank_id


    # Events

    def __on_avatar_become_player(self):
        self.__state = BankManager.STATE_BATTLE
        self.__banks_reload()


    def __on_avatar_become_non_player(self):
        self.__state = BankManager.STATE_HANGAR
        self.__banks_reload()

    # Utils

    def fix_path_slashes(self, path):
        """
        Replaces backslashes with slashes
        """

        if path:
            path = path.replace('\\', '/')
            if path[-1] != '/':
                path += '/'

        return path


    def resolve_path(self, path, basepath=None):
        """
        Resolves path to file

        'xvm://*' --> '../res_mods/mods/shared_resources/xvm/*'
        'res://*' --> '../res_mods/mods/shared_resources/*'
        'cfg://*' --> '../res_mods/configs/xvm/*'
        '*'       --> 'basepath/*'
        """

        if path[:6].lower() == "res://":
            path = path.replace("res://", xfw_loader.XFWLOADER_PATH_TO_ROOT + "res_mods/mods/shared_resources/", 1)
        elif path[:6].lower() == "xvm://":
            path = path.replace("xvm://", xfw_loader.XFWLOADER_PATH_TO_ROOT + "res_mods/mods/shared_resources/xvm/", 1)
        elif path[:6].lower() == "cfg://":
            path = path.replace("cfg://", xfw_loader.XFWLOADER_PATH_TO_ROOT + "res_mods/configs/xvm/", 1)
        elif basepath:
            path = self.fix_path_slashes(basepath) + path

        return path.replace('\\', '/')


    def __normalize_path(self, path):
        """
        Normalize path to sound bank:

        cfg://* -> ../res_mods/configs/xvm/*
        res://* -> ../res_mods/mods/shared_resources/*
        xvm://* -> ../res_mods/mods/shared_resources/xvm/*
        *       -> ../res_mods/x.x.x/audioww/*
        """
        return self.resolve_path(path, xfw_loader.WOT_RESMODS_DIR + '/audioww/').lower()



#
# Initialization
#

def init():
    global g_xfw_wwise_bankmanager
    global g_xfw_wwise_bankmanager_inited
    g_xfw_wwise_bankmanager = BankManager(native.wwise_native())
    if g_xfw_wwise_bankmanager is not None:
        g_xfw_wwise_bankmanager_inited = True


def fini():
    global g_xfw_wwise_bankmanager
    global g_xfw_wwise_bankmanager_inited
    
    if g_xfw_wwise_bankmanager is not None:
        g_xfw_wwise_bankmanager.__fini__()
        g_xfw_wwise_bankmanager = None

    g_xfw_wwise_bankmanager_inited = False


def is_inited():
    global g_xfw_wwise_bankmanager_inited
    return g_xfw_wwise_bankmanager_inited



#
# Public
#

def wwise_bank_add(bank_path, add_to_battle, add_to_hangar): # -> Bool
    if not is_inited():
        return False
    return g_xfw_wwise_bankmanager.bank_add(bank_path, add_to_battle, add_to_hangar)


def wwise_bank_remove(bank_path): # -> Bool
    if not is_inited():
        return False
    return g_xfw_wwise_bankmanager.bank_remove(bank_path)


def wwise_bank_unload(bank_name): # -> Bool
    if not is_inited():
        return False
    return g_xfw_wwise_bankmanager.bank_unload(bank_name)
