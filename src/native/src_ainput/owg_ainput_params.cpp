// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

//
// Includes
//

// OpenWG.WoT.Audio
#include "owg_ainput_params.h"



//
// Implementation
//

namespace OpenWG::Audio::AInput {
    AInputParams::AInputParams() {
    }

    AInputParams::AInputParams(const AInputParams &in_rCopy) {
    }

    AInputParams::~AInputParams() {
    }

    //
    // AK::IAkRTPCSubscriber
    //

    AKRESULT AInputParams::SetParam(AkPluginParamID in_ParamID,
                                    const void *in_pValue,
                                    AkUInt32 in_ulParamSize) {
        if (!in_pValue) {
            return AK_InvalidParameter;
        }

        switch (in_ParamID) {
            default:
                return AK_InvalidParameter;
        }

        return AK_Success;
    }

    //
    // AK::IAkPluginParam
    //

    AK::IAkPluginParam *AInputParams::Clone(AK::IAkPluginMemAlloc *in_pAllocator) {
        return AK_PLUGIN_NEW(in_pAllocator, AInputParams(*this));
    }

    AKRESULT AInputParams::Init(AK::IAkPluginMemAlloc *in_pAllocator, const void *in_pParamsBlock,
                                AkUInt32 in_ulBlockSize) {
        if (in_ulBlockSize == 0) {
            return AK_Success;
        }

        return SetParamsBlock(in_pParamsBlock, in_ulBlockSize);
    }

    AKRESULT AInputParams::Term(AK::IAkPluginMemAlloc *in_pAllocator) {
        AK_PLUGIN_DELETE(in_pAllocator, this);
        return AK_Success;
    }

    AKRESULT AInputParams::SetParamsBlock(const void *in_pParamsBlock, AkUInt32 in_ulBlockSize) {
        return AK_Success;
    }

} // namespace OpenWG::Audio::AInput
