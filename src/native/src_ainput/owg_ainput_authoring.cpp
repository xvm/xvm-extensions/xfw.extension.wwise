// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

//
// Includes
//

// OpenWG.WoT.Audio
#include "owg_ainput_authoring.h"



//
// Implementation
//

namespace OpenWG::Audio::AInput {
	AInputAuthoringPlugin::AInputAuthoringPlugin() {
	}

	AInputAuthoringPlugin::~AInputAuthoringPlugin() {
	}

	bool AInputAuthoringPlugin::GetBankParameters(const GUID &in_guidPlatform,
		AK::Wwise::Plugin::DataWriter &in_dataWriter) const {
		return true;
	}
}
