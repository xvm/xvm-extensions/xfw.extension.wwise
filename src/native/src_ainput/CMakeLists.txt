add_library(owg_ainput SHARED)

target_sources(owg_ainput PRIVATE
    owg_ainput_authoring.cpp
    owg_ainput_plugin.cpp
    owg_ainput_params.cpp
    owg_ainput_main.cpp
)

target_include_directories(owg_ainput PUBLIC ../3rdparty/ak_2023.1.4/include)