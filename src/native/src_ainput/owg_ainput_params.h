// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

#pragma once

//
// Includes
//

// stdlib
#include <cmath>

// Audiokinetic
#include <AK/SoundEngine/Common/IAkPlugin.h>



//
// Class
//

namespace OpenWG::Audio::AInput {
	class AInputParams : public AK::IAkPluginParam
	{
	public:
		AInputParams();
		AInputParams( const AInputParams & in_rCopy );
		~AInputParams() override;

		// AK::IAkRTPCSubscriber
		AKRESULT SetParam(AkPluginParamID in_ParamID, const void * in_pValue, AkUInt32 in_ulParamSize) override;

		// AK::IAkPluginParam
		IAkPluginParam * Clone( AK::IAkPluginMemAlloc * in_pAllocator ) override;
		AKRESULT Init(AK::IAkPluginMemAlloc *	in_pAllocator, const void *in_pParamsBlock, AkUInt32 in_ulBlockSize	) override;
		AKRESULT Term( AK::IAkPluginMemAlloc * in_pAllocator ) override;
		AKRESULT SetParamsBlock( const void * in_pParamsBlock, AkUInt32 in_ulBlockSize) override;
	};
}
