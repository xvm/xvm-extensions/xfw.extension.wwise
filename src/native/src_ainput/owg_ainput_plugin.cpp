// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

//
// Includes
//

// Audiokinetic
#include <AK/AkWwiseSDKVersion.h>

// OpenWG.WoT.Audio
#include "owg_ainput_plugin.h"



//
// Implementation
//


namespace OpenWG::Audio::AInput {
	AInputPlugin::AInputPlugin() {

	}

	AInputPlugin::~AInputPlugin() {
	}


	//
	// AK::IAkPlugin
	//

	AKRESULT AInputPlugin::Term(AK::IAkPluginMemAlloc *in_pAllocator) {
		AK_PLUGIN_DELETE( in_pAllocator, this );
		return AK_Success;
	}

	AKRESULT AInputPlugin::Reset() {
		return AK_Success;
	}

	AKRESULT AInputPlugin::GetPluginInfo(AkPluginInfo &out_rPluginInfo) {
		out_rPluginInfo.eType = AkPluginTypeSource;
		out_rPluginInfo.bIsInPlace = true;
		out_rPluginInfo.uBuildVersion = AK_WWISESDK_VERSION_COMBINED;
		return AK_Success;
	}

	bool AInputPlugin::SupportMediaRelocation() const {
		return IAkSourcePlugin::SupportMediaRelocation();
	}

	AKRESULT AInputPlugin::RelocateMedia(AkUInt8 *in_pNewMedia, AkUInt8 *in_pOldMedia) {
		return IAkSourcePlugin::RelocateMedia(in_pNewMedia, in_pOldMedia);
	}




	//
	// AK::IAkSourcePlugin
	//

	AKRESULT AInputPlugin::Init(AK::IAkPluginMemAlloc *in_pAllocator, AK::IAkSourcePluginContext *in_pSourceFXContext,
	AK::IAkPluginParam *in_pParams, AkAudioFormat &io_rFormat) {
		// Keep source FX context (looping info etc.)
		m_pSourceFXContext = in_pSourceFXContext;

		// Set parameters access
		m_pSharedParams = reinterpret_cast<AInputParams *>(in_pParams);
		m_Format = io_rFormat;

		return AK_Success;
	}

	AkReal32 AInputPlugin::GetDuration() const {
		return 0.0f;
	}

	AkReal32 AInputPlugin::GetEnvelope() const {
		return IAkSourcePlugin::GetEnvelope();
	}

	AKRESULT AInputPlugin::StopLooping() {
		return IAkSourcePlugin::StopLooping();
	}

	AKRESULT AInputPlugin::Seek(AkUInt32 in_uPosition) {
		return IAkSourcePlugin::Seek(in_uPosition);
	}

	AKRESULT AInputPlugin::TimeSkip(AkUInt32 &ak_u_int32) {
		return IAkSourcePlugin::TimeSkip(ak_u_int32);
	}

	void AInputPlugin::Execute(AkAudioBuffer *io_pBuffer) {
		//TODO
		//m_pfnExecCallback( m_pSourceFXContext->GetVoiceInfo()->GetPlayingID(), io_pBufferOut);
	}
}
