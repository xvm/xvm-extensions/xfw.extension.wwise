// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

#pragma once

//
// Includes
//

// Audiokinetic
#include <AK/Wwise/Plugin.h>

namespace OpenWG::Audio::AInput {
	class AInputAuthoringPlugin: public AK::Wwise::Plugin::AudioPlugin
	{
	public:
		AInputAuthoringPlugin();
		~AInputAuthoringPlugin();

		bool GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::Plugin::DataWriter& in_dataWriter ) const override;
	};
}
