// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

#pragma once


//
// Defines
//

#define AKCOMPANYID_OPENWG 1001
#define AKPLUGINID_OPENWG_AINPUT 10001
