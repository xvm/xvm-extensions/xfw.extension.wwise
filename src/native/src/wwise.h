// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2024 XVM Team
#pragma once

//
// Includes
//

// stdlib
#include <cstdint>
#include <filesystem>
#include <string>
#include <vector>



//
// Class
//
class Wwise{
    // Ctor
public:
    Wwise();

    // BankLoad
public:
    uint32_t BankLoad(std::wstring path_bank, std::wstring path_audioww);

private:
    uint32_t bankLoadRFS(std::filesystem::path path_bank, std::wstring path_audioww);
    
    uint32_t bankLoadVFS(std::wstring path_bank);


    // BankUnload
public:
    bool BankUnload(uint32_t bank_id);

    bool BankUnloadByName(std::wstring bank_name);


    // Communication
public:
    bool CommunicationInit();


    // File
private:
    bool fileExistsRFS(const std::filesystem::path& filepath);

    bool fileExistsVFS(const std::wstring& filepath);

    std::vector<uint8_t> fileLoadRFS(const std::filesystem::path& filepath);

    std::vector<uint8_t> fileLoadVFS(const std::wstring& filepath);

    // ResMgr
private:
    pybind11::module m_resmgr;
};
