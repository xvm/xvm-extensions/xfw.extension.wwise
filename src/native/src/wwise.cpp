// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2024 XVM Team

//
// Includes
//

// stdlib
#include <fstream>

// pybind11
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

// Audiokinetic
#include <AK/Comm/AkCommunication.h>
#include <AK/SoundEngine/Common/AkSoundEngine.h>

// XFW.WWise
#include "fileHelper.h"
#include "wwise.h"



//
// Ctor
// 

Wwise::Wwise()
{
    pybind11::gil_scoped_acquire acquire;
    m_resmgr = pybind11::module::import("ResMgr");
}



//
// Bank/Load
//

uint32_t Wwise::BankLoad(std::wstring path_bank, std::wstring path_audioww) {
    uint32_t result{ 0U };

    //check that WWise is OK
    if (!AK::SoundEngine::IsInitialized()) {
        pybind11::print("[WWise/BankLoad]    ERROR: Sound engine is not initialized");
        return 0;
    }

    // load from RFS
    result = bankLoadRFS(path_bank, path_audioww);
    if (result) {
        return result;
    }

    // load from VFS
    result = bankLoadVFS(path_bank);
    if (result) {
        return result;
    }

    pybind11::print("[WWise/BankLoad]: bank file was not found:", path_bank.c_str());
    return 0;
}


uint32_t Wwise::bankLoadRFS(std::filesystem::path path_bank, std::wstring path_audioww)
{
    // extract bankname
    std::wstring bank_name{ std::filesystem::path(path_bank).filename().wstring() };

    //check that file exists
    auto path_flavor = path_bank.replace_extension().concat("_").concat(CLIENT_FLAVOR).concat(".bnk");
    if (fileExistsRFS(path_flavor)) {
        path_bank = path_flavor;
    }
    else if (!fileExistsRFS(path_bank)) {
        return 0;
    }

    // create target directory if not exists
    auto path_dir = std::filesystem::absolute(path_audioww);
    if (!std::filesystem::exists(path_dir)) {
        std::filesystem::create_directories(path_dir);
    }

    // check that directory was actually created
    if (!std::filesystem::exists(path_dir)) {
        pybind11::print("[WWise/bankLoadRFS] failed to create audioww directory");
        return 0;
    }

    // check existing file and remove if does not match
    auto path_new = path_dir / bank_name;
    if (std::filesystem::exists(path_new)) {
        if (!FileHelper::AreEqual(path_bank, path_new)) {
            if (!std::filesystem::remove(path_new)) {
                pybind11::print("[WWise/bankLoadRFS] failed to remove file");
                return 0;
            }
        }
    }

    // create hardlink
    if (!std::filesystem::exists(path_new)) {
        if (!FileHelper::CreateHardlink(path_bank, path_new)) {
            std::filesystem::copy(path_bank, path_new);
            if (!std::filesystem::exists(path_new)) {
                pybind11::print("[WWise/bankLoadRFS] failed to copy file");
                return 0;
            }
        }
    }

    //load bank
    AkBankID bank_id = 0;

    // attempt_1, FS
    AKRESULT return_code = AK::SoundEngine::LoadBank(bank_name.c_str(), bank_id);
    if (return_code == AK_Success) {
        pybind11::print("[WWise/bankLoadRFS]    INFO : Loaded via FS");
        return bank_id;
    }

    // attempt_2, Memory
    if (return_code == AK_FileNotFound) {
        //read bank to memory
        auto filebuf = fileLoadRFS(std::filesystem::absolute(path_bank));
        if (!filebuf.size()) {
            pybind11::print("[WWise/bankLoadRFS]    ERROR: Failed to read file");
            return 0;
        }

        //load bank from memory
        return_code = AK::SoundEngine::LoadBankMemoryCopy(filebuf.data(), filebuf.size(), bank_id);
        if (return_code == AK_Success) {
            pybind11::print("[WWise/bankLoadRFS]    INFO : Loaded via LoadBankMemoryCopy");
            return bank_id;
        }
    }

    pybind11::print("[WWise/bankLoadRFS] Failed to load bank, error code", static_cast<int32_t>(return_code));
    return 0;
}


uint32_t Wwise::bankLoadVFS(std::wstring path_bank)
{
    //check that file exists
    auto path_flavor = FileHelper::RemoveExtension(path_bank) + L"_" + CLIENT_FLAVOR + L".bnk";
    if (fileExistsVFS(path_flavor)) {
        path_bank = path_flavor;
    }
    else if (!fileExistsVFS(path_bank)) {
        return 0;
    }

    AkBankID bank_id = 0;

    // read content
    auto filebuf = fileLoadVFS(path_bank);
    if (!filebuf.size()) {
        pybind11::print("[WWise/bankLoadVFS]    ERROR: Failed to read file");
        return 0;
    }

    // load bank
    AKRESULT return_code = AK::SoundEngine::LoadBankMemoryCopy(filebuf.data(), filebuf.size(), bank_id);
    if (return_code != AK_Success) {
        pybind11::print("[WWise/bankLoadVFS] Failed to load bank, error code", static_cast<int32_t>(return_code));
        return 0;

    }

    pybind11::print("[WWise/bankLoadVFS]    INFO : Loaded via LoadBankMemoryCopy");
    return bank_id;
}



//
// Bank/Unload
//

bool Wwise::BankUnload(uint32_t bank_id) {
    pybind11::print("[XFW_WWISE/BankUnload]: bank_id=", bank_id);

    if (!AK::SoundEngine::IsInitialized()) {
        pybind11::print("[XFW_WWISE/BankUnload]    ERROR: Sound engine is not initialized");
        return false;
    }

    AKRESULT return_code = AK::SoundEngine::UnloadBank(bank_id, nullptr);
    if (return_code != AK_Success) {
        pybind11::print("[XFW_WWISE/Bank_Unload]   ERROR: Audiokinetic error code: ", static_cast<int32_t>(return_code));
        return false;
    }

    pybind11::print("[XFW_WWISE/Bank_Unload]    INFO : Unloaded");
    return true;
}

bool Wwise::BankUnloadByName(std::wstring bank_name) {
    if (!AK::SoundEngine::IsInitialized()) {
        pybind11::print("[WWise/BankUnloadByName]    ERROR: Sound engine is not initialized");
        return false;
    }

    AKRESULT return_code = AK::SoundEngine::UnloadBank(bank_name.c_str(), nullptr);
    if (return_code != AK_Success) {
        pybind11::print("[WWise/BankUnloadByName]   ERROR: Audiokinetic error code: ", static_cast<int32_t>(return_code));
        return false;
    }

    return true;
}



//
// Communication 
//


bool Wwise::CommunicationInit() {
    AkCommSettings commSettings{};
    AK::Comm::GetDefaultInitSettings(commSettings);

    AKRESULT return_code = AK::Comm::Init(commSettings);
    if (return_code != AK_Success) {
        pybind11::print("[WWise/CommunicationInit]    ERROR: Audiokinetic error code: ", static_cast<int32_t>(return_code));
        return false;
    }

    return true;
}



// 
//
// File/Exists
//

bool Wwise::fileExistsRFS(const std::filesystem::path& filepath)
{
    return std::filesystem::exists(filepath);
}

bool Wwise::fileExistsVFS(const std::wstring& filepath)
{
    pybind11::gil_scoped_acquire acquire;

    pybind11::bool_ res = m_resmgr.attr("isFile")(filepath);
    if (res.is_none()) {
        return false;
    }

    return res;
}



//
// File/Load
//


std::vector<uint8_t> Wwise::fileLoadRFS(const std::filesystem::path& filepath)
{
    if (!fileExistsRFS(filepath)) {
        return {};
    }

    std::ifstream file(filepath, std::ios::binary);
    if (!file.is_open()) {
        return {};
    }

    //unset skipws
    file.unsetf(std::ios::skipws);

    // get its size:
    file.seekg(0, std::ios::end);
    std::streampos fileSize{ file.tellg() };
    file.seekg(0, std::ios::beg);

    // reserve capacity
    std::vector<uint8_t> vec;
    vec.reserve(fileSize);

    // read the data:
    vec.insert(vec.begin(),
        std::istream_iterator<uint8_t>(file),
        std::istream_iterator<uint8_t>());

    //close file
    file.close();

    return vec;
}

std::vector<uint8_t> Wwise::fileLoadVFS(const std::wstring& filepath)
{
    pybind11::gil_scoped_acquire acquire;

    auto section = m_resmgr.attr("openSection")(filepath);
    if (section.is_none()) {
        return {};
    }

    pybind11::object binary = section.attr("asBinary");
    if (binary.is_none()) {
        return {};
    }

    char* binary_buf{};
    ssize_t binary_len{};

    int binary_errcode = PyString_AsStringAndSize(binary.ptr(), &binary_buf, &binary_len);
    if (binary_errcode != 0) {
        return {};
    }

    std::vector<uint8_t> vec(binary_len);
    std::memcpy(vec.data(), binary_buf, binary_len);

    return vec;
}
