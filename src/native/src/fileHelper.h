// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2022 XVM Team

#pragma once

#include <filesystem>

namespace FileHelper {

    /// <summary>
    /// Checks that two files have the same size and modification time
    /// </summary>
    /// <param name="first">path to the first file</param>
    /// <param name="second">path to the second file</param>
    /// <returns>true in case of equal files</returns>
    bool AreEqual(const std::filesystem::path& first, const std::filesystem::path& second);

    /// <summary>
    /// Create hardlink between existing file and target path
    /// </summary>
    /// <param name="path_from">existing file path</param>
    /// <param name="path_to">target file path</param>
    /// <returns>true in case of sucessful hardlink creation</returns>
    bool CreateHardlink(const std::filesystem::path& path_from, const std::filesystem::path& path_to);

    /// <summary>
    /// Checks that file is placed on NTFS filesystem
    /// </summary>
    /// <param name="path">relative or absolute path to the file</param>
    /// <returns>True in case of NTFS filesystem</returns>
    bool OnNTFS(const std::filesystem::path& path);

    /// <summary>
    /// Remove extension from filepath
    /// </summary>
    /// <param name="str">input filepath</param>
    /// <returns>Filepath without extension</returns>
    std::wstring RemoveExtension(const std::wstring& str);
};