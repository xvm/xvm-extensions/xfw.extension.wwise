# Changelog

## v11.0.1

* MT 1.32.0 support

## v11.0.0

* MT 1.30.0 support

## v10.3.0

* WoT 1.26.0 support

## v10.2.1

* fix native initialization

## v10.2.0

* add `bank_unload_from_name`
* add ability to load bank from VFS

## v10.1.0

* fix build


## v10.0.0

* WoT 1.18.0 support

## v1.0.2

* change the World of Tanks linkage method

## v1.0.1

* banks loading fixes
* improved logging

## v1.0.0

* initial version